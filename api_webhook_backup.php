<?php 
function process_message($message)
{
    //memproses response
    include_once './class_response.php';
    $respon = new class_response($message);
    $respon->get_response();
}
$entityBody = file_get_contents('php://input');
$message = json_decode($entityBody, true);
process_message($message);
?>