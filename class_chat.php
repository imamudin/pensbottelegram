<?php

class class_chat{
    public $id;
    public $first_name;
    public $last_name;
    public $type;
            
    function __construct($chat) {
        $this->id          = $chat['id'];
        $this->first_name  = $chat['first_name'];
        $this->last_name   = $chat['last_name'];
        $this->type        = $chat['type'];
    }
    function __destruct() {
        ;
    }
}