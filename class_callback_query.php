<?php
include_once("./class_message.php");
include_once("./class_from.php");
class class_callback_query {
    public $id;
    public $from;
    public $message;
    public $data;
            
    function __construct($result) {
        $this->id       = $result['id'];
        $this->message  = new class_message($result['message']);
        $this->from     = new class_from($result['from']);
        $this->data     = $result['data'];
    }
 
    function __destruct() {
 
    } 
}
?>