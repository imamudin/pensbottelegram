<?php

class class_update_message {

    public $chat_id;
    public $message_id;
    public $text;

    function __construct($chat_id, $message_id,  $text) {
        $this->chat_id = $chat_id;
        $this->message_id = $message_id;
        $this->text = $text;
    }

    function setChat_id($chat_id) {
        $this->chat_id = $chat_id;
    }

    function setMessage_id($message_id) {
        $this->message_id = $message_id;
    }

    function setText($text) {
        $this->text = $text;
    }

    function update() {
        include 'token.php';
        
        $reply = "?chat_id=" . $this->chat_id . "&message_id=" . $this->message_id. "&text=" . $this->text;
        
        $result = file_get_contents("https://api.telegram.org/bot" . $TOKEN . "/editMessageText" . $reply);
        print_r($result);
    }
}

//untuk mengupdate chat login
// include_once './class_update_message.php';
// $update_message = new class_update_message($this->message->chat->id, $this->message->message_id, 'text');
// $update_message->update();
