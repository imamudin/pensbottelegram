<?php 
include("token.php");
function request_url($method)
{
    global $TOKEN;
    return "https://api.telegram.org/bot" . $TOKEN . "/". $method;
}
 
function send_reply($chatid, $msgid, $text)
{
    $data = array(
        'chat_id' => $chatid,
        'text'  => $text,
        'reply_to_message_id' => $msgid
 
    );
    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );
    $context  = stream_context_create($options);
 
    $result = file_get_contents(request_url('sendMessage'), false, $context);
    print_r($result);
}
function mysend_reply($chatid, $msgid, $text)
{
    $result = file_get_contents(request_url('sendMessage')."?chat_id=".$chatid."&text=".$text);
    print_r($result);
}
 
function create_response($text)
{
	$response = "";
	switch($text){
		case "/start":
			$response = "Selamat Datang di PENSBot";
			break;
		case "/nilai":
			$response = "Nilai Database : 100";
			break;
		default :
			$response = "Respons tidak diketahui";
	}
   return $response;
}
 
 
function process_message($message)
{
	$db_nilai = array(
		"Teori Database I : 90",
		"Praktikum Database I : 86",
		"Pemrograman Perangkat Lunak : 79",
		"Manajemen Proyek Sistem Informasi : 85",
		"Keamanan Jaringan : 93",
		"Pemodelan Data : 92",
	);
    $updateid = $message["update_id"];
    $message_data = $message["message"];
    if (isset($message_data["text"])) {
    $chatid = $message_data["chat"]["id"];
        $message_id = $message_data["message_id"];
        $text = $message_data["text"];
		for($i=0;$i<count($db_nilai);$i++){
			$response	= $db_nilai[$i];
			mysend_reply($chatid, $message_id, $response);
		}
    }
    return $updateid;
}
$entityBody = file_get_contents('php://input');
$message = json_decode($entityBody, true);
process_message($message);
?>