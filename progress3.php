<?php

include("token.php");

function request_url($method) {
    global $TOKEN;
    return "https://api.telegram.org/bot" . $TOKEN . "/" . $method;
}

function get_updates($offset) {
    $url = request_url("getUpdates") . "?offset=" . $offset;
    $resp = file_get_contents($url);
    $result = json_decode($resp, true);
    if ($result["ok"] == 1)
        return $result["result"];
    return array();
}

function process_respon($respon) {
    //memproses response
    include_once './class_response.php';
    $respon = new class_response($respon);
    $respon->get_response();
    
    return $respon->update_id;
}

function process_one() {
    $update_id = 0;

    if (file_exists("last_update_id")) {
        $update_id = (int) file_get_contents("last_update_id");
    }

    $updates = get_updates($update_id);

    foreach ($updates as $respon) {
        $update_id = process_respon($respon);
    }
    file_put_contents("last_update_id", $update_id + 1);
}

while (true) {
    process_one();
}
?>