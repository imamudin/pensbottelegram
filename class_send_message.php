<?php

class class_send_message {

    public $chat_id;
    public $text;
    public $parse_mode = "HTML";
    public $reply_to_message_id;
    public $reply_markup;

    function __construct($chat_id, $text, $parse_mode, $reply_to_message_id, $reply_markup) {
        $this->chat_id = $chat_id;
        $this->text = $text;
        $this->parse_mode = $parse_mode;
        $this->reply_to_message_id = $reply_to_message_id;
        $this->reply_markup = $reply_markup;
    }

    function setChat_id($chat_id) {
        $this->chat_id = $chat_id;
    }

    function setText($text) {
        $this->text = $text;
    }

    function setParse_mode($parse_mode) {
        $this->parse_mode = $parse_mode;
    }

    function setReply_to_message_id($reply_to_message_id) {
        $this->reply_to_message_id = $reply_to_message_id;
    }

    function setReply_markup($reply_markup) {
        $this->reply_markup = $reply_markup;
    }

    function send() {
        include 'token.php';
        
        $reply = "?chat_id=" . $this->chat_id . "&text=" . $this->text . "&parse_mode=" . $this->parse_mode . ""
                ."&reply_to_message_id=" . $this->reply_to_message_id;
        
        if($this->reply_markup!=''){
            $reply  = $reply. "&reply_markup=" . json_encode($this->reply_markup);
        }
        $result = file_get_contents("https://api.telegram.org/bot" . $TOKEN . "/sendMessage" . $reply);
        print_r("https://api.telegram.org/bot" . $TOKEN . "/sendMessage" . $reply);
        print_r($result);
    }
}
