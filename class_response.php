<?php

$respons_text = array(
    1   => "Selamat datang di Sistem Layanan Akademik PENS. \npowered by PENS"
);

class class_response {

    public $buttons;
    public $update_id;
    public $input;

    function __construct($input) {
        $this->update_id = $input['update_id'];
        $this->input = $input;
    }

    function get_response() {
        if (isset($this->input["message"])) {
            include_once './class_message.php';
            $message = new class_message($this->input['message']);
            if (isset($message->text)) {
                $response = new response_message($message);
                $response->process_response_message();
            }
        }
        //untuk callback_query
        else if (isset($this->input["callback_query"])) {
            include_once './class_callback_query.php';
            $callback   = new class_callback_query($this->input['callback_query']);
            if (isset($callback->data)) {
                $response = new response_callback_query($callback);
                $response->process_response_callback_query();
            }
        }
    }

}

class response_message {
    public $respons_text = array(
        1   => "Selamat datang di Sistem Layanan Akademik PENS. \npowered by PENS",
        2   => "Berikut cara penggunaan simpensbot : \n1. Pilih menu Dapatkan ID. Sistem kemudian akan membalas ID untuk Anda. \n 2. Silakan login ke online.mis.pens.ac.id. Pilih menu Setting dan masukan ID tersebut pada kolom telegram id kemudian simpan.\n3. Kembali pada chat telegram simpensbot dan pilih menu verifikasi. \n4. Simpensbot sudah bisa digunakan.",
        4   => "Silakan masukan token dengan mengawalinya dengan garis miring (/). Misal /UqdAN9.",
        5   => "Format tidak diketahui",
        6   => "Selamat datang <b> nama </b>.\nNikmati layanan Akademik PENS melalui Telegram Bot Messenger.",
        7   => "Mohon maaf email dan key tidak cocok.",
        8   => "Anda login sebagai Wali dari Mahasiswa <b>nama</b>",
        9   => "Hasil UMPENS tidak ditemukan. \nSilakan coba lagi.",
        10  => "Mohon Maaf data tidak ditemukan.",
        11  => "Silakan pilih Semester",
        13  => "Pengumuman tidak ditemukan.",
        14  => "Tidak ada balasan dari server.",
        25  => "Respon tidak ditemukan.",
        29  => "Silakan masukan ID ke online.mis.pens.ac.id",
        30  => "Anda telah masuk dengan akun lain, silakan masuk kembali."
    );
    
    public $buttons = array(
        "nilai"             => "Nilai",
        "kehadiran"         => "Kehadiran",
        "jadwal_kuliah"     => "Jadwal Kuliah",
        "pengumuman_terbaru"=> "Pengumuman Terbaru",
        "penggunaan"        => "Bantuan",
        "wali"              => "Wali Mahasiswa",
        "get_id"            => "Dapatkan ID",
        "verifikasi"        => "Verifikasi",
        "status"            => "Status",
        "kembali"           => "Kembali",
        "matakuliah"        => "Matakuliah",
        "" => "",
    );
    public $message;        #obj of class_message

    function __construct($message) {
        $this->message = $message;
    }

    function process_response_message() {
        //class untuk mengirim pesan
        include_once './class_send_message.php';
        $respon = "";
        $markup = "";
        $send_message = new class_send_message($this->message->chat->id, $respon, "HTML", $this->message->message_id, $markup);
        
        //print_r("button".$buttons['login_mahasiswa']);
        $text   = $this->message->text;
        
        $texts  = explode("/",$text);
        //untuk menghapus spasi
        for($i=0;$i<count($texts);$i++){
            $texts[$i] = trim($texts[$i]);
        }
        //echo "                           jumlah:   ".count($texts)."                 ".$texts[0]."                             ";
        
        if(count($texts)>0){
            //merubah menjadi kapital
            $texts[0] =  strtoupper($texts[0]);
            //echo "                         ".$texts[0]."                  ".$text."                 ";

            //khusus untuk login wali karena dengan garis mirin (/)
            $panjang_token = 6; //untuk panjang token pada database
            if(count($texts)==2){
                $text   =  strtoupper($text);
                if ($text == "/START") {
                    $data = array(
                        "telegram_id"   => $this->message->from->id
                    );
                    //mengecek apakah user telah login
                    $nama = $this->is_login($this->message->from->id);
                    if($nama){
                        //sudah login
                        $respon = urlencode(str_replace("nama", $nama, $this->respons_text[6]));

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['nilai']
                            ),
                            array(
                                $this->buttons['kehadiran']
                            ),
                            array(
                                $this->buttons['jadwal_kuliah']
                            ),
                            array(
                                $this->buttons['pengumuman_terbaru']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();
                    }else{
                        //belum login
                        //kirim pesan selamat datang
                        $respon = urlencode($this->respons_text[1]);

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['penggunaan']
                            ),
                            array(
                                $this->buttons['get_id']
                            ),
                            array(
                                $this->buttons['verifikasi']
                            ),
                            array(
                                $this->buttons['wali']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();
                    }
                }else if(strlen($texts[1])==$panjang_token){
                    //melakukan login untuk wali mahasiswa
                    //$respon = urlencode($this->respons_text[1]);
                    $data = array(
                        "token"         => $texts[1],
                        "telegram_id"   => $this->message->from->id
                    );

                    // //echo '                        '.json_encode($data)."                               ";
                    $login  = $this->CallAPI("PUT", "/api/loginwalinew/imamudin", json_encode($data));

                    $data_login = json_decode($login, true);

                    if($data_login['status']==1){       //untuk yg berhasil login biasa
                        //sudah login
                        $respon = urlencode($this->respons_text[1]);

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['status']
                            ),
                            array(
                                $this->buttons['nilai']
                            ),
                            array(
                                $this->buttons['kehadiran']
                            ),
                            array(
                                $this->buttons['jadwal_kuliah']
                            ),
                            array(
                                $this->buttons['pengumuman_terbaru']
                            ),
                            array(
                                $this->buttons['kembali']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();

                        if($data_login['id_lama'] != ''){ //untuk yang telah login sebelumnya pada hape yang lain
                            //mengirim ke telegram sebelumnya untuk login lagi, dan merubah menu

                            $send_notif = new class_send_message($this->message->chat->id, $respon, "HTML", $this->message->message_id, $markup);
                            $send_notif->setText($this->respons_text[30]);
                            $send_notif->setReply_to_message_id('');
                            $send_notif->send();
                        }
                    }else{
                        $respon = urlencode($data_login['message']);
                    }
                }else{
                    $respon = urlencode($this->respons_text[25]);
                }
            }else{
                $text   =  strtoupper($text);
                //untuk pesan tanpa format garing '/'
                if ($text == strtoupper($this->buttons['penggunaan'])) {
                    $respon = urlencode($this->respons_text[2]);
                }else if ($text == strtoupper($this->buttons['get_id'])) {
                    $respon = urlencode("ID anda adalah ".$this->message->from->id.". Silakan masukan ID ke online.mis.pens.ac.id pada menu Setting.");
                }else if ($text == strtoupper($this->buttons['verifikasi'])){
                    //mengecek apakah user telah login
                    $nama = $this->is_login($this->message->from->id);
                    if($nama){
                        //sudah login
                        $respon = urlencode(str_replace("nama", $nama, $this->respons_text[6]));

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['nilai']
                            ),
                            array(
                                $this->buttons['kehadiran']
                            ),
                            array(
                                $this->buttons['jadwal_kuliah']
                            ),
                            array(
                                $this->buttons['pengumuman_terbaru']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();
                    }else{
                        //belum login
                        $respon = urlencode($this->respons_text[29]);
                    }
                }else if ($text == strtoupper($this->buttons['wali'])){
                    //mengecek apakah user telah login
                    if($this->is_login_wali($this->message->from->id)){
                        //sudah login
                        $respon = urlencode($this->respons_text[1]);

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['status']
                            ),
                            array(
                                $this->buttons['nilai']
                            ),
                            array(
                                $this->buttons['kehadiran']
                            ),
                            array(
                                $this->buttons['jadwal_kuliah']
                            ),
                            array(
                                $this->buttons['pengumuman_terbaru']
                            ),
                            array(
                                $this->buttons['kembali']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();
                    }else{
                        //belum login
                        $respon = urlencode($this->respons_text[4]);
                    }
                }else if ($text == strtoupper($this->buttons['status'])){
                    $id_user    = $this->message->from->id;

                    $data = $this->CallAPI("GET", "/api/statuswali/".$id_user);
                    $data = json_decode($data, true);

                    if($data['status']==1){
                        $nama   = $data['data'];
                        if($nama == ''){
                            $respon = urlencode($this->respons_text[10]);  
                        }else{
                            $respon = urlencode(str_replace("nama", $nama, $this->respons_text[8]));
                        }
                    }else{
                        //jika tidak ada respon
                        $respon = urlencode($this->respons_text[14]);
                    }
                }else if ($text == strtoupper($this->buttons['kembali'])){
                    $id_user    = $this->message->from->id;

                    $data = $this->CallAPI("GET", "/api/walikembali/".$id_user);
                    $data = json_decode($data, true);

                    if($data['status']==1){
                        //merubah menu kembali ke awal
                        $respon = urlencode($this->respons_text[1]);

                        //set reply keyboard markup
                        $keyboard = array(
                            array(
                                $this->buttons['penggunaan']
                            ),
                            array(
                                $this->buttons['get_id']
                            ),
                            array(
                                $this->buttons['verifikasi']
                            ),
                            array(
                                $this->buttons['wali']
                            )
                        );
                        include_once './class_reply_keyboard_markup.php';
                        $btn_keyboard = new class_reply_keyboard_markup($keyboard, true, true, "");

                        $markup = $btn_keyboard->get_reply_ketboard_markup();
                    }else{
                        //jika tidak ada respon
                        $respon = urlencode($this->respons_text[14]);
                    }
                }else if ($text == strtoupper($this->buttons['nilai'])){
                    $respon = urlencode($this->respons_text[11]);

                    //pembuatan inline button
                    include_once './class_inline_keyboard.php';

                    $test = new class_inline_keyboard();
                    for ($i = 1; $i <= 8; $i++) {
                        $test->add_button(new class_inline_keyboard_button("".$i, "", $this->buttons['nilai']."/" . $i, ""));
                    }

                    $markup = $test->array_inline_keyboard;

                }else if ($text == strtoupper($this->buttons['kehadiran'])){
                    $respon = urlencode($this->respons_text[11]);

                    //pembuatan inline button
                    include_once './class_inline_keyboard.php';

                    $test = new class_inline_keyboard();
                    for ($i = 1; $i <= 8; $i++) {
                        $test->add_button(new class_inline_keyboard_button("".$i, "", $this->buttons['kehadiran']."/" . $i, ""));
                    }

                    $markup = $test->array_inline_keyboard;

                }else if ($text == strtoupper($this->buttons['jadwal_kuliah'])){
                    $respon = urlencode($this->respons_text[11]);

                    //pembuatan inline button
                    include_once './class_inline_keyboard.php';

                    $test = new class_inline_keyboard();
                    for ($i = 1; $i <= 8; $i++) {
                        $test->add_button(new class_inline_keyboard_button("".$i, "", $this->buttons['jadwal_kuliah']."/" . $i, ""));
                    }

                    $markup = $test->array_inline_keyboard;

                }else if ($text == strtoupper($this->buttons['pengumuman_terbaru'])){
                    $data = $this->CallAPI("GET", "/api/pengumumannew/1");
                    $data = json_decode($data, true);
                    //var_dump($data);
                    if($data['status']==1){
                        for ($i = 0; $i < count($data['content']); $i++) {
                            $dari   = $data['content'][$i]['NAMA'];
                            $judul  = $data['content'][$i]['JUDUL'];
                            $tanggal= $data['content'][$i]['DIBUAT'];
                            $isi    = $data['content'][$i]['URAIAN'];

                            $pengumuman = urlencode('<b>'.$judul."</b>\n"
                                    . "<b>dari :</b> ".$dari."\n"
                                    . "<b>tanggal :</b> ".$tanggal."\n"
                                    . "<b>pengumuman :</b> ".$isi."\n");

                            $pengumumans[] = $pengumuman;
                        }
                        $send_pengumuman = new class_send_message($this->message->chat->id, $respon, "HTML", $this->message->message_id, $markup);
                        for($i=0;$i<count($pengumumans)-1;$i++){
                            $send_pengumuman->setText($pengumumans[$i]);
                            $send_pengumuman->setReply_to_message_id('');
                            $send_pengumuman->send();
                        }
                        $respon = $pengumumans[(count($pengumumans)-1)];

                        //pembuatan inline button
                        include_once './class_inline_keyboard.php';

                        $test = new class_inline_keyboard();
                        $test->add_button(new class_inline_keyboard_button("Lainnya", "", $this->buttons['pengumuman_terbaru']."/3", ""));

                        $markup = $test->array_inline_keyboard;
                        $send_message->setReply_to_message_id("");
                    }else{
                        $respon = urlencode($this->respons_text[13]);
                    }
                }else{
                    $respon = urlencode($this->respons_text[25]);
                }
            }         
        }else{
            $respon = urlencode($this->respons_text[25]);
        }
        $send_message->setReply_markup($markup);
        $send_message->setText($respon);
        $send_message->send();
    }
    function is_login($telegram_id){
        $data = array(
            "telegram_id"   => $telegram_id
        );
        //mengecek apakah user telah login
        $login  = $this->CallAPI("PUT", "/api/isloginmahasiswanew/imamudin", json_encode($data));
        $data_login = json_decode($login, true);
        //echo '                                             '.$login.'                                ';
        if($data_login['status']==1){
            return $data_login['data'];
        }else{
            return false;
        }
    }
    function is_login_wali($telegram_id){
        $data = array(
            "telegram_id"   => $telegram_id
        );
        //mengecek apakah user telah login
        $login  = $this->CallAPI("PUT", "/api/isloginwalinew/imamudin", json_encode($data));
        $data_login = json_decode($login, true);
        //echo '                                             '.$login.'                                ';
        if($data_login['status']==1){
            return true;
        }else{
            return false;
        }
    }
    function CallAPI($method, $url, $data = false) {
        include 'token.php';
        $url    = $API_MISPENS. $url;
        $curl   = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); // note the PUT here
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}

class response_callback_query {
    public $callback_query; #obj of class_callback_query
    
    public $respons_text = array(
        1   => "Mengambil nilai di Semester",
        2   => "Mengambil kehadiran di Semester",
        3   => "Mengambil jadwal kuliah di Semester",
        4   => "Mengambil pengumuman di Semester",
        5   => "Respon tidak ditemukan",
        6   => "Data Pengumuman tidak ditemukan lagi.",
        7   => "Data Nilai <b>Semester null</b> tidak ditemukan.",
        8   => "Jadwal Kuliah <b>Semester null</b>  tidak ditemukan.",
        9   => "Anda belum login, silakan melakukan login.",
        10  => "Data Kehadiran <b>Semester null</b>  tidak ditemukan.",
        11  => "Tidak ada balasan dari server.",
    );

    function __construct($callback_query) {
        $this->callback_query = $callback_query;
    }

    function process_response_callback_query() {
        $message_response = new response_message("");
        include_once './class_send_message.php';
        $respon = "";
        $markup = "";
        $send_message = new class_send_message($this->callback_query->message->chat->id, $respon, "HTML", $this->callback_query->message->message_id, $markup);
        
        $data   = $this->callback_query->data;
        $datas  = explode("/", $data);
        //untuk menghapus spasi
        for($i=0;$i<count($datas);$i++){
            $datas[$i] = trim($datas[$i]);
        }
        
        //if($message_response->is_login($this->callback_query->from->id)){
            if(count($datas)>0){
                if(count($datas)==2){
                    if($datas[0]==$message_response->buttons['nilai']){
                        $respon = urlencode($this->respons_text[1]." ".$datas[1]);

                        $id_user    = $this->callback_query->from->id;
                        $semester   = $datas[1];

                        $dataa = $message_response->CallAPI("GET", "/api/nilainew/".$id_user."/".$semester);
                        $data = json_decode($dataa, true);
                        if($data['status']==1){
                            $respon = urlencode($data['data']);

                            if($data['total']>=0){
                                //pembuatan inline button
                                include_once './class_inline_keyboard.php';

                                $test = new class_inline_keyboard();
                                for ($i = 1; $i <= 8; $i++) {
                                    $test->add_button(new class_inline_keyboard_button("".$i, "", $message_response->buttons['nilai']."/" . $i, ""));
                                }

                                $markup = $test->array_inline_keyboard;
                            }
                        }else{
                            //jika tidak ada respon
                            $respon = urlencode($this->respons_text[11]);
                        }
                        //echo '                        kk         '.$dataa."      ds                           ";
                    }else if($datas[0]==$message_response->buttons['kehadiran']){                
                        $id_user    = $this->callback_query->from->id;
                        $semester   = $datas[1];

                        $data = $message_response->CallAPI("GET", "/api/getmatakuliah/".$id_user."/".$semester);
                        $data = json_decode($data, true);

                        if($data['status']==1){
                            $respon = urlencode("Silakan pilih Mata Kuliah : \n");

                            if($data['total']>=0){
                                //pembuatan inline button
                                include_once './class_inline_keyboard.php';

                                $test = new class_inline_keyboard();
                                $matakuliahs = $data['data'];
                                for($i=0;$i <count($matakuliahs);$i++){
                                    $matakuliah     = $matakuliahs[$i];
                                    $nama_matkul    = str_replace("&", "dan", $matakuliah['MATAKULIAH']);
                                    $test->add_row(new class_inline_keyboard_button("".$nama_matkul, "", $message_response->buttons['matakuliah']."/".$matakuliah['NOMOR'], ""));
                                }

                                $markup = $test->array_inline_keyboard;
                            }
                        }else{
                            //jika tidak ada respon
                            $respon = urlencode($this->respons_text[11]);
                        }
                    }else if($datas[0]==$message_response->buttons['matakuliah']){                
                        $id_user    = $this->callback_query->from->id;
                        $nomor_matakuliah   = $datas[1];

                        $data = $message_response->CallAPI("GET", "/api/kehadiranperminggu/".$id_user."/".$nomor_matakuliah);
                        $data = json_decode($data, true);

                        if($data['status']==1){
                            $respon = urlencode($data['data']);
                        }else{
                            //jika tidak ada respon
                            $respon = urlencode($this->respons_text[11]);
                        }
                    }else if($datas[0]==$message_response->buttons['jadwal_kuliah']){
                        $id_user    = $this->callback_query->from->id;
                        $semester   = $datas[1];

                        $data = $message_response->CallAPI("GET", "/api/jadwalkuliahnew/".$id_user."/".$semester);
                        $data = json_decode($data, true);

                        //echo "                                                    /api/jadwal_kuliah/".$id_user."/".$semester."                                                  ";

                        if ($data['total'] > 0) {
                            $haris = array();
                            //$nilais = "<b>Jadwal Kuliah Semester $semester </b>\n";

                            $haris[] = new hari("Minggu");
                            $haris[] = new hari("Senin");
                            $haris[] = new hari("Selasa");
                            $haris[] = new hari("Rabu");
                            $haris[] = new hari("Kamis");
                            $haris[] = new hari("Jumat");
                            $haris[] = new hari("Sabtu");


                            for ($i = 0; $i < count($data['data']); $i++) {
                                $hari = $data['data'][$i]['HARI'];
                                $dosen = $data['data'][$i]['DOSEN'];
                                $ruang = $data['data'][$i]['RUANG'];
                                $waktu = $data['data'][$i]['WAKTU'];
                                $durasi = $data['data'][$i]['JAM'];
                                $matakuliah = $data['data'][$i]['MATAKULIAH'];
                                //echo $hari;
                                $haris[$hari]->add_jadwal(new jadwal($matakuliah, $dosen, $ruang, $waktu, $durasi));
                            }
                            $jadwal ="";
                            for($i=0;$i<count($haris);$i++){
                                if(count($haris[$i]->jadwal)==0){
                                    continue;
                                }
                                $jadwal = $jadwal . $haris[$i]->toString();
                            }
                            $respon = urlencode("<b>Jadwal Kuliah Semester $semester </b>\n\n".$jadwal);
                            if($data['total']>=0){
                                //pembuatan inline button
                                include_once './class_inline_keyboard.php';

                                $test = new class_inline_keyboard();
                                for ($i = 1; $i <= 8; $i++) {
                                    $test->add_button(new class_inline_keyboard_button("".$i, "", $message_response->buttons['jadwal_kuliah']."/" . $i, ""));
                                }

                                $markup = $test->array_inline_keyboard;
                            }
                        } else {
                            $respon = $data['data'];
                        }
                    }else if($datas[0]==$message_response->buttons['pengumuman_terbaru']){
                        if($message_response->is_login($this->callback_query->from->id)){
                            //$respon = urlencode($this->respons_text[4]." ".$datas[1]);

                            $data = $message_response->CallAPI("GET", "/api/pengumumannew/".$datas[1]);
                            $data = json_decode($data, true);
                            //var_dump($data);
            //                print_r("                                                   hasil : ".($data['status'])."                                                 ");
                            if($data['status']==1){
                                for ($i = 0; $i < count($data['content']); $i++) {
                                    $dari   = $data['content'][$i]['NAMA'];
                                    $judul  = $data['content'][$i]['JUDUL'];
                                    $tanggal= $data['content'][$i]['DIBUAT'];
                                    $isi    = $data['content'][$i]['URAIAN'];

                                    $pengumuman = urlencode('<b>'.$judul."</b>\n"
                                            . "<b>dari :</b> ".$dari."\n"
                                            . "<b>tanggal :</b> ".$tanggal."\n"
                                            . "<b>pengumuman :</b> ".$isi."\n");

                                    $pengumumans[] = $pengumuman;
                                }
                                $send_pengumuman = new class_send_message($this->callback_query->message->chat->id, $respon, "HTML", $this->callback_query->message->message_id, $markup);
                                for($i=0;$i<count($pengumumans)-1;$i++){
                                    $send_pengumuman->setText($pengumumans[$i]);
                                    $send_pengumuman->setReply_to_message_id('');
                                    $send_pengumuman->send();
                                }
                                $respon = $pengumumans[(count($pengumumans)-1)];

                                //pembuatan inline button
                                include_once './class_inline_keyboard.php';

                                $test = new class_inline_keyboard();
                                $test->add_button(new class_inline_keyboard_button("Lainnya", "", $message_response->buttons['pengumuman_terbaru'].'/'.($datas[1]+3), ""));

                                //print_r("                                                   hasil : ".($datas[1]+3)."                                                 ");

                                $markup = $test->array_inline_keyboard;
                                $send_message->setReply_to_message_id("");

                            }else{
                                $respon = urlencode($this->respons_text[6]);
                            }
                        }else{
                            $respon = urlencode($this->respons_text[5]);
                        }
                    }else{
                        $respon = urlencode($this->respons_text[5]);
                    }
                }
            }else{
                $respon = urlencode($this->respons_text[5]);
            }
//        }else{
//            $respon = urlencode($this->respons_text[9]);
//        }
        
        $send_message->setReply_markup($markup);
        $send_message->setReply_to_message_id("");
        $send_message->setText($respon);
        $send_message->send();
    }
}

class hari{
    public $hari;
    public $jadwal = array();
    
    function __construct($hari) {
        $this->hari =   $hari;
    }
    function add_jadwal($jadwal){
        $this->jadwal[] = $jadwal;
    }
    function get_jadwal(){
        return $this->jadwal[0];
    }
    function toString(){
        $hari = "<b>-".$this->hari."</b>\n".
                "---------------------------------------------------------\n";
        for($i=0;$i<count($this->jadwal);$i++){
            $hari = $hari.$this->jadwal[$i]->toString();
        }
        return $hari;
    }
}

class jadwal {

    public $matakuliah;
    public $dosen;
    public $ruang;
    public $waktu;
    public $durasi;

    function __construct($matakuliah, $dosen, $ruang, $waktu, $durasi) {
        $this->matakuliah = $matakuliah;
        $this->dosen = $dosen;
        $this->ruang = $ruang;
        $this->waktu = $waktu;
        $this->durasi = $durasi;
    }

    function toString() {
        $jadwal = '<b>'.$this->matakuliah . "</b>\n" .
                "<b>".$this->ruang . "</b>, " . $this->waktu . " (" . $this->durasi . "jam)\n".
                $this->dosen . " \n".
                "---------------------------------------------------------\n";
        return $jadwal;
    }
}
