<?php

class class_reply_keyboard_markup {

    public $keyboard = array();
    public $resize_keyboard;
    public $on_time_keyboard;
    public $selective;

    function __construct($keyboard, $resize_keyboard, $on_time_keyboard, $selective) {
        $this->keyboard         = $keyboard;
        $this->resize_keyboard  = $resize_keyboard;
        $this->on_time_keyboard = $on_time_keyboard;
        $this->selective        = $selective;
    }

    function add_keyboard($keyboard) {
        $this->keyboard[] = array($keyboard);
    }

    function get_reply_ketboard_markup() {
        return array(
            "keyboard"          => $this->keyboard,
            "resize_keyboard"   => true,
            "one_time_keyboard" => true,
        );
    }

}

class class_keyboard_button {

    public $text;

    function __construct($text) {
        $this->text = $text;
    }

}
//
//header('Content-Type: application/json');
//
//$keyboard = array(
//    array(
//        "Nilai",
//    ),
//    array(
//        "Jadwal",
//    ),
//    array(
//        "Absen"
//    )
//);
//
////untuk ReplyKeyboardMarkup
//$resp = array(
//    "keyboard"          => $keyboard,
//    "resize_keyboard"   => true,
//    "one_time_keyboard" => true,
//);
//$reply = json_encode($resp, JSON_PRETTY_PRINT);
//
//echo $reply;
//
//$btn_keyboard = new class_reply_keyboard_markup("", true, true,"");
//
//$btn1 = new class_keyboard_button("Nilai");
//$btn2 = new class_keyboard_button("Jadwal");
//$btn3 = new class_keyboard_button("Absensi");
//
//$btn_keyboard->add_keyboard($btn1->text);
//$btn_keyboard->add_keyboard($btn2->text);
//$btn_keyboard->add_keyboard($btn3->text);
//
//$reply = json_encode($btn_keyboard->get_reply_ketboard_markup(), JSON_PRETTY_PRINT);
//
//echo $reply;
