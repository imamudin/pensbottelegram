<?php

class class_from{
    public $id;
    public $first_name;
    public $last_name;
    public $username;
            
    function __construct($from){
        $this->id          = $from['id'];
        $this->first_name  = $from['first_name'];
        
        if(isset($from['last_name'])) $this->last_name   = $from['last_name'];
        else $this->last_name   = "";
        if(isset($from['username'])) $this->username   = $from['username'];
        else $this->username   = "";
    }
    function __destruct() {
    }
}