<?php

class class_inline_keyboard {
    public $inline_keyboard = array();  //type must array of inline keyboard button
    public $array_inline_keyboard = array();
    public $row_array_inline_keyboard = array();
    
    function __construct() {
    }
    function setInline_keyboard($inline_keyboard) {
        $this->inline_keyboard = $inline_keyboard;
        $this->array_inline_keyboard = array(
            "inline_keyboard" => array($this->inline_keyboard),
        );
    }

    function add_button($inline_keyboard_button){
        $this->inline_keyboard[] = $inline_keyboard_button->button;
        $this->array_inline_keyboard = array(
            "inline_keyboard" => array($this->inline_keyboard),
        );
    }

    function add_row($inline_keyboard_button){
        $this->row_array_inline_keyboard[] = array(
            $inline_keyboard_button->button
        );
        
        $this->array_inline_keyboard = array(
            "inline_keyboard" => $this->row_array_inline_keyboard,
        );
    }
}
class class_inline_keyboard_button{
    public $button;
            
    function __construct($text, $url, $callback_data,$switch_inline_query) {
        $this->button = array(
            "text"                  => $text,
            "url"                   => $url,
            "callback_data"         => $callback_data,
            "switch_inline_query"   => $switch_inline_query,
        );
    }
}