<?php
include_once("./class_from.php");
include_once("./class_chat.php");

class class_message {
    public $message_id;
    public $from;
    public $chat;
    public $date;
    public $text;
        
    function __construct($result) {
        $this->message_id   = $result['message_id'];
        $this->from         = new class_from($result['from']);
        $this->chat         = new class_chat($result['chat']);
        $this->date         = $result['date'];
        $this->text         = $result['text'];
    }
 
    function __destruct() {
 
    }
}
?>